<?php
require_once 'libcalcul.php';

if ($argc>3) {
    $somme=$argv[1];
    $taux=$argv[2];
    $duree=$argv[3];
}
else{
    $somme=0;
    $taux=1;
    $duree=1;
}

echo "cumul= $somme *( 1 + $taux/100 ) ^ $duree";
echo " = ";
echo cumul($somme,$taux,$duree);
echo "\n";
?>