<html>

<head>
    <title>Multiplication:</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel='stylesheet' href='multi.css'>
</head>

<body>
    <h1>Multiplication:</h1>
    <!-- method peut prendre les valeurs "get" et "post" -->
    <form method="get" action="multi.php">
        <label for="l">Nombre de ligne</label> <input type="text" id="l" name="l" /> <br />
        <label for="c">Nombre de colonne</label> <input type="text" id="c" name="c" /> <br />
        <label for="choix">Ligne à surligner</label> <input type="text" id="choix" name="choix" />
        <input type="submit" />
    </form>


    <?php
    if (isset($_GET["l"])) {
        $l = $_GET["l"];
    }
    if (isset($_GET["c"])) {
        $c = $_GET["c"];
    }
    if (isset($_GET["choix"])) {
        $choix = $_GET["choix"];
    } else {
        $l = 10;
        $c = 10;
        $choix = 0;
    }
    ?>

    <table>
        <thead>
            <tr>
                <?php
                echo "<th>x</th>";
                for ($i = 0; $i <= $c; $i++) {
                    echo "<th>$i</th>";
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i <= $c; $i++) {
                if ($i == $choix) {
                    echo "<tr class=surligne>";
                } else {
                    echo "<tr>";
                }
                echo "<th>$i</th>";
                for ($j = 0; $j <= $l; $j++) {
                    $tmp = $i * $j;
                    echo "<td>$tmp</td>";
                }
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</body>

</html>